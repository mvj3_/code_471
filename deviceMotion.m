//初始化CoreMotion
#import <CoreMotion/CoreMotion.h>  
   
CMMotionManager *motionManager = [[CMMotionManager alloc]init];  
//1. Accelerometer 获取手机加速度数据
CMAccelerometerData *newestAccel = motionManager.accelerometerData;  
double accelerationX = newestAccel.acceleration.x;  
double accelerationY = newestAccel.acceleration.y;  
double accelerationZ = newestAccel.acceleration.z;  
 
//2. Gravity 获取手机的重力值在各个方向上的分量，根据这个就可以获得手机的空间位置，倾斜角度等
double gravityX = motionManager.deviceMotion.gravity.x;  
double gravityY = motionManager.deviceMotion.gravity.y;  
double gravityZ = motionManager.deviceMotion.gravity.z;  
 
//获取手机的倾斜角度：
double zTheta = atan2(gravityZ,sqrtf(gravityX*gravityX+gravityY*gravityY))/M_PI*180.0;  
   
double xyTheta = atan2(gravityX,gravityY)/M_PI*180.0;  
 
//zTheta是手机与水平面的夹角， xyTheta是手机绕自身旋转的角度
 
//3. DeviceMotion 获取陀螺仪的数据 包括角速度，空间位置等
//旋转角速度：
CMRotationRate rotationRate = motionManager.deviceMotion.rotationRate;  
double rotationX = rotationRate.x;  
double rotationY = rotationRate.y;  
double rotationZ = rotationRate.z;  
 
//空间位置的欧拉角（通过欧拉角可以算得手机两个时刻之间的夹角，比用角速度计算精确地多）
double roll    = motionManager.deviceMotion.attitude.roll;  
double pitch   = motionManager.deviceMotion.attitude.pitch;  
double yaw     = motionManager.deviceMotion.attitude.yaw;  
 
//空间位置的四元数（与欧拉角类似，但解决了万向结死锁问题）
double w = motionManager.deviceMotion.attitude.quaternion.w;  
double wx = motionManager.deviceMotion.attitude.quaternion.x;  
double wy = motionManager.deviceMotion.attitude.quaternion.y;  
double wz = motionManager.deviceMotion.attitude.quaternion.z;  
 
 
//通过陀螺仪模块可以实现模拟赛车，模拟射击等。
